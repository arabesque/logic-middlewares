import * as tape from "tape";
import {Middleware} from "@arabesque/core";
import {spy} from "sinon";
import {createNOTMiddleware} from "../../../../src";

const createInputMiddleware = (input: boolean): Middleware<any> => {
    return (context, next) => {
        return input ? next(context) : Promise.resolve(context);
    };
};

tape('NOT', ({test}) => {
    const output = spy((context, next) => {
        return next(context);
    });

    const testCases: Array<{
        input: boolean;
        expectation: boolean
    }> = [
        {
            input: false,
            expectation: true
        },
        {
            input: true,
            expectation: false
        }
    ];

    for (let {input, expectation} of testCases) {
        const middleware = createInputMiddleware(input);

        const candidate = createNOTMiddleware(middleware);

        test(`${input} -> ${expectation}`, ({end, same}) => {
            return candidate('foo', (context) => {
                return output(context, () => Promise.resolve());
            }).then(() => {
                same(output.callCount, expectation ? 1 : 0);

                output.resetHistory();

                end();
            });
        });
    }
});