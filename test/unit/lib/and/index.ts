import * as tape from "tape";
import {Middleware} from "@arabesque/core";
import {spy} from "sinon";
import {createANDMiddleware} from "../../../../src/lib/and";

const createInputMiddleware = (input: boolean): Middleware<any> => {
    return (context, next) => {
        return input ? next(context) : Promise.resolve(context);
    };
};

tape('AND', ({test}) => {
    const output = spy((context, next) => {
        return next(context);
    });

    const testCases: Array<{
        inputs: Array<boolean>;
        expectation: boolean
    }> = [
        {
            inputs: [false],
            expectation: false
        },
        {
            inputs: [true],
            expectation: true
        },
        {
            inputs: [true, false],
            expectation: false
        },
        {
            inputs: [true, true],
            expectation: true
        }
    ];

    for (let {inputs, expectation} of testCases) {
        const middlewares = inputs.map(createInputMiddleware);

        const candidate = createANDMiddleware(...middlewares);

        test(`${inputs} -> ${expectation}`, ({end, same}) => {
            return candidate('foo', (context) => {
                return output(context, () => Promise.resolve());
            }).then(() => {
                same(output.callCount, expectation ? 1 : 0);

                output.resetHistory();

                end();
            });
        });
    }

    test('is lazy', ({end, same}) => {
        const firstMiddleware = spy(createInputMiddleware(true));

        const secondMiddleware = spy(createInputMiddleware(false));

        const thirdMiddleware = spy(createInputMiddleware(false));

        const candidate = createANDMiddleware(firstMiddleware, secondMiddleware, thirdMiddleware);

        return candidate('foo', () => Promise.resolve()).then(() => {
            same(firstMiddleware.callCount, 1, 'should execute first middleware');
            same(secondMiddleware.callCount, 1, 'should execute second middleware');
            same(thirdMiddleware.callCount, 0, 'should not execute third middleware');

            output.resetHistory();

            end();
        });
    });
});