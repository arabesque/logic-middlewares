# Arabesque Logic Middlewares

[![NPM version][npm-image]][npm-url] [![Coverage Status][coverage-image]][coverage-url]

## What a middleware is

A middleware is conceptually a switch: it receives a signal and either lets it pass or halts it. Concretely, an Arabesque middleware receives a context and either passes it to the next middleware (by resolving it through the `next` callback) or halts its (by resolving it outside the `next` callback).

```ts
// a middleware that passes the context
const pass: Middleware<any> = (context, next) => {
    return next(context);
};

// a middleware that halts the context
const halt: Middleware<any> = (context) => {
    return Promise.resolve(context);
};
```

Said differently: middlewares are logic gates.

## Provided middleware factories

This package provides a set of middlewares that implements four basic logic gates: AND, OR, NOT and XOR.

### createANDMiddleware

```ts
declare const createANDMiddleware: <Context>(middleware: Middleware<Context>, ...middlewares: Middleware<Context>[]) => Middleware<Context>;
```

Returns a middleware that lets the context pass if all the passed middlewares let it pass, and halts it if at least one of the passed middlewares halts it. Lazy, will stop as soon as it is able to resolve its behavior.

> Said differently: returns a AND Gate.

### createORMiddleware

```ts
declare const createORMiddleware: <Context>(middleware: Middleware<Context>, ...middlewares: Middleware<Context>[]) => Middleware<Context>;
```

Returns a middleware that lets the context pass if at least one of the passed middlewares lets it pass, and halts it if all the passed middlewares halt it. Lazy, will stop as soon as it is able to resolve its behavior.

> Said differently: returns a OR Gate.

### createNOTMiddleware

```ts
declare const createNOTMiddleware: <Context>(middleware: Middleware<Context>) => Middleware<Context>;
```

Returns a middleware that lets the context pass if the passed middleware halts it, and halts it if the passed middleware lets it pass.

> Said differently: returns a NOT Gate.

### createXORMiddleware

```ts
declare const createXORMiddleware: <Context>(middleware: Middleware<Context>, ...middlewares: Middleware<Context>[]) => Middleware<Context>;
```

Returns a middleware that lets the context pass if exactly one of the passed middlewares lets it pass, and halts it if none or more than one of the passed middlewares lets it pass. Lazy, will stop as soon as it is able to resolve its behavior.

> Said differently: returns a XOR Gate.

## Contributing

* Fork this repository
* Code
* Implement tests using [tape](https://github.com/substack/tape)
* Issue a merge request keeping in mind that all pull requests must reference an issue in the issue queue

[npm-image]: https://badge.fury.io/js/@arabesque%2Flogic-middlewares.svg
[npm-url]: https://www.npmjs.com/package/@arabesque/logic-middlewares
[coverage-image]: https://coveralls.io/repos/gitlab/arabesque/logic-middlewares/badge.svg
[coverage-url]: https://coveralls.io/gitlab/arabesque/logic-middlewares
