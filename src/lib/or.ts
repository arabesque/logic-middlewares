import {Middleware} from "@arabesque/core";

/**
 * Create a "OR" middleware.
 *
 * https://en.wikipedia.org/wiki/OR_gate
 */
export const createORMiddleware = <Context>(...middlewares: Array<Middleware<Context>>): Middleware<Context> => {
    return (context, next) => {
        let opened: boolean = false;

        const invokeMiddlewares = (context: any, middlewares: Array<Middleware<Context>>): Promise<Context> => {
            const middleware = middlewares[0];

            if (!middleware) {
                return Promise.resolve(context);
            }

            return middleware(context, (context) => {
                opened = true;

                return Promise.resolve(context);
            }).then((context) => {
                return opened ? Promise.resolve(context) : invokeMiddlewares(context, middlewares.slice(1));
            });
        };

        return invokeMiddlewares(context, middlewares).then((context) => {
            return opened ? next(context) : Promise.resolve(context);
        });
    };
}