import {Middleware} from "@arabesque/core";

/**
 * Create a "AND" middleware.
 *
 * https://en.wikipedia.org/wiki/AND_gate
 */
export const createANDMiddleware = <Context>(...middlewares: Array<Middleware<Context>>): Middleware<Context> => {
    return (context, next) => {
        let opened: boolean = false;

        const invokeMiddlewares = (context: any, middlewares: Array<Middleware<Context>>): Promise<Context> => {
            const middleware = middlewares[0];

            if (!middleware) {
                return Promise.resolve(context);
            }

            opened = false;

            return middleware(context, (context) => {
                opened = true;

                return invokeMiddlewares(context, middlewares.slice(1));
            });
        };

        return invokeMiddlewares(context, middlewares).then((context) => {
            return opened ? next(context) : Promise.resolve(context);
        });
    };
}